/**
 * Created by liubit on 2016/12/12.
 * 忘记密码-获取验证码
 */
import React from 'react';
import { View, Text, TextInput, TouchableOpacity,
	Image,
    StyleSheet,
    ToastAndroid,
} from 'react-native';

import ToolBarView from '../../widget/ToolBarView';
import ForgetPwResetPage from './ForgetPwResetPage';

class ForgetPwGetCodePage extends React.Component {
	goto(index){
		if(index==1){
            ToastAndroid.show('获取验证码', ToastAndroid.SHORT);
		}else if(index==2){
            this.props.navigator.push({name:ForgetPwResetPage});
		}
	}

	render() {
		return (
			<View style={styles.container}>
				<ToolBarView title={'忘记密码'}
							 navigator={this.props.navigator}/>
				<View style={{width:300,flexDirection:'row',alignItems:'flex-end'}}>
					<TextInput
						placeholder={'手机号'}
						style={{ height: 40, width: 200,marginTop:50 }} />

					<TouchableOpacity onPress={()=>this.goto(1)}>
						<View style={[styles.comfirmBtn,{padding:8,}]}>
							<Text style={{fontSize:20}}>
								获取验证码
							</Text>
						</View>
					</TouchableOpacity>
				</View>

				<TextInput
					placeholder={'验证码'}
					style={{ height: 40, width: 300,marginTop:20,alignSelf:'center' }} />

				<Text style={{margin:20,marginTop:40}}>
					若长时间无法收到，请检查是否被手机安全软件拦截
				</Text>

				<TouchableOpacity onPress={()=>this.goto(2)}>
					<View style={[styles.comfirmBtn,{marginTop:40,width:250}]}>
						<Text style={{fontSize:20,margin:15}}>
							确认
						</Text>
					</View>
				</TouchableOpacity>


			</View>
		);
	}
}

let styles = StyleSheet.create({
    container:{
        flex: 1, alignItems: 'center', backgroundColor: '#FFFFFF',
        flexDirection:'column',
    },
    comfirmBtn:{
        backgroundColor:'#f0f0f0',
        borderRadius:5,
		marginRight:20,
		justifyContent:'center',
		flexDirection:'row'
    },



});

export default ForgetPwGetCodePage;