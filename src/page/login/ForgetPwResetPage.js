/**
 * Created by liubit on 2016/12/12.
 * 忘记密码-重置密码
 */
import React from 'react';
import { View, Text, TextInput, TouchableOpacity,
	Image,
    StyleSheet,
    ToastAndroid,
} from 'react-native';

import ToolBarView from '../../widget/ToolBarView';

class ForgetPwResetPage extends React.Component {
	goto(){

	}

	render() {
		return (
			<View style={styles.container}>
				<ToolBarView title={'忘记密码'}
							 navigator={this.props.navigator}/>
				<TextInput
					placeholder={'请输入新密码'}
					style={{ height: 40, width: 250,marginTop:50,alignSelf:'center' }} />

				<TextInput
					placeholder={'请再次输入新密码'}
					style={{ height: 40, width: 250,marginTop:20,alignSelf:'center' }} />

				<TouchableOpacity onPress={()=>this.goto(2)}>
					<View style={[styles.comfirmBtn,{marginTop:40,width:250}]}>
						<Text style={{fontSize:20,margin:15}}>
							完成
						</Text>
					</View>
				</TouchableOpacity>


			</View>
		);
	}
}

let styles = StyleSheet.create({
    container:{
        flex: 1, alignItems: 'center', backgroundColor: '#FFFFFF',
        flexDirection:'column',
    },
    comfirmBtn:{
        backgroundColor:'#f0f0f0',
        borderRadius:5,
		marginRight:20,
		justifyContent:'center',
		flexDirection:'row'
    },



});

export default ForgetPwResetPage;