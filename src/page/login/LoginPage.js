/**
 * Created by liubit on 2016/12/12.
 * 登录界面
 */
import React from 'react';
import { View, Text, TextInput, TouchableOpacity,
	Image,
    StyleSheet,
    ToastAndroid,
    Platform,
} from 'react-native';

import ForgetPwGetCodePage from './ForgetPwGetCodePage';

let login = 1;
let rigister = 2;
let forgetPw = 3;
let logoPaddingTop = 30;

class LoginPage extends React.Component {
    componentWillMount(){
        if (Platform.OS === 'ios') {
            logoPaddingTop = 65;
        }
    }

	goto(index){
		switch (index){
		case login:
            ToastAndroid.show('login', ToastAndroid.SHORT);
			break;
		case rigister:
            ToastAndroid.show('rigister', ToastAndroid.SHORT);
			break;
		case forgetPw:
            this.props.navigator.push({name:ForgetPwGetCodePage});
			break;
		default:
			break;
		}
	}

	render() {
		return (
			<View style={styles.container}>
				<Image style={{width:150,height:150,marginTop:logoPaddingTop}}
					   source={require('../../../res/drawable/ic_launcher.png')}/>

				<TextInput
					placeholder={'请输入用户名/手机号/邮箱'}
					style={{ height: 40, width: 250,marginTop:50,alignSelf:'center' }} />

				<TextInput
					placeholder={'请输入密码'}
					style={{ height: 40, width: 250,marginTop:20,alignSelf:'center' }} />

				<TouchableOpacity onPress={()=>this.goto(login)}>
					<View style={[styles.comfirmBtn,{marginTop:40}]}>
						<Text style={{fontSize:20,margin:15}}>
							确认
						</Text>
					</View>
				</TouchableOpacity>

				<View style={{justifyContent:'center',flexDirection:'row',marginTop:20}}>
					<TouchableOpacity onPress={()=>this.goto(rigister)}>
						<Text style={{color:'#2bf0e7'}}>注册新用户</Text>
					</TouchableOpacity>

					<View style={{width:1.5,backgroundColor:'#2bf0e7',
						marginRight:20,marginLeft:20}}/>

					<TouchableOpacity onPress={()=>this.goto(forgetPw)}>
						<Text style={{color:'#2bf0e7'}}>忘记密码</Text>
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}

let styles = StyleSheet.create({
    container:{
        flex: 1, alignItems: 'center', backgroundColor: '#FFFFFF',
        flexDirection:'column',
    },
    comfirmBtn:{
        margin:20,backgroundColor:'#f0f0f0',
        justifyContent:'center',borderRadius:5,flexDirection:'row',
		width:300,
    },



});

export default LoginPage;