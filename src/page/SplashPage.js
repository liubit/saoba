import React from 'react';
import { View, Text } from 'react-native';

import HomePage from './main/HomePage';

class SplashPage extends React.Component {
    componentDidMount() {
		this.timer = setTimeout(
			()=>{
                this.props.navigator.replace({name: HomePage});
			},
			10
		);
    }

	render() {
		return (
			<View style={{ flex: 1, alignItems: 'center',backgroundColor: '#FFFFFF' }}>
				<Text style={{marginTop:100,fontSize:40}}>Splash Page</Text>

			</View>
		);
	}
}

export default SplashPage;