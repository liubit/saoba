/**
 * Created by liubit on 2016/12/7.
 * 输金额界面
 */
import React from 'react';
import { TextInput, View, Text, TouchableOpacity,
    StyleSheet,
    ToastAndroid,
} from 'react-native';


import ToolBarView from '../../widget/ToolBarView';
import QRCodePage from './QRCodePage';
import HandleCardPage from './HandleCardPage';
const dismissKeyboard = require('dismissKeyboard');
let type = 0;

class PaymentPage extends React.Component{
    goto(){
        dismissKeyboard();
        if(this.props.type==1){
            this.props.navigator.push({
                name: QRCodePage,
                params:{
                    text:this.state.money,
                }
            });
        }else if(this.props.type==2){
            this.props.navigator.push({name:HandleCardPage});
        }
    }

    componentWillMount() {
        this.setState({
            type: this.props.type,
            money: '0',
        });
    }

    componentWillUnMount() {
        dismissKeyboard();
    }

    render() {
        return (
            <View style={styles.container}>
                <ToolBarView title={this.props.title}
                             navigator={this.props.navigator}/>

                <View style={{alignItems:'center',flexDirection:'column',}}>
                    <Text style={{fontSize:20,margin:30}}>
                        输金额
                    </Text>
                </View>

                <View style={{flexDirection:'column'}}>
                    <TextInput style={styles.textInput}
                               autoFocus={true}
                               underlineColorAndroid='transparent'
                               placeholder={'0.00'}
                               keyboardType={'numeric'}
                               maxLength={12}
                               textAlign={'center'}
                               onChangeText={(text)=>this.setState({money: text})}/>
                </View>

                <TouchableOpacity onPress={()=>this.goto()}>
                    <View style={[styles.comfirmBtn,{marginTop:40}]}>
                        <Text style={{fontSize:20,margin:15}}>
                            确认
                        </Text>
                    </View>
                </TouchableOpacity>

            </View>
        )
    }

}

let styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#FFFFFF' ,
        flexDirection:'column',

    },
    textInput:{
        height:80,
        fontSize:60,

    },
    comfirmBtn:{
        margin:20,backgroundColor:'#f0f0f0',
        justifyContent:'center',borderRadius:5,flexDirection:'row'
    }


});

export default PaymentPage;
