/**
 * Created by liubit on 2016/12/6.
 * 主界面
 */
import React from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet,
	DrawerLayoutAndroid,
	ToastAndroid,
    BackAndroid,
} from 'react-native';

import PercentageCircle from 'react-native-percentage-circle';
import NavigationView from '../../widget/NavigationView';
import ChargedetailPage from './ChargedetailPage';
import MsgCenterPage from './MsgCenterPage';
import PaymentPage from './PaymentPage';
import ScanViewPage from './ScanViewPage';
import NetUtil from '../../tools/NetUtil';

var UIHelper = require('../../tools/UIHelper');
let Dimensions = require('Dimensions');
let DRAWER_REF = 'drawer';
let openDrawer = 0;
let chargedetail = 1;
let logout = 2;
let msgcenter = 3;
let payment = 4;
let card = 5;
let scanview = 6;
let navigater;

class HomePage extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            drawer: false,
        };
    }

    close_drawer(){
    	this.setState({
            drawer:true,
		})
	}

    componentDidUpdate() {
        if(this.state.drawer){
            this.refs[DRAWER_REF].closeDrawer();
        }
    }

	componentWillMount() {
		BackAndroid.addEventListener('hardwareBackPress', this.onBackAndroid);
    }

    componentWillUnMount() {
		BackAndroid.removeEventListener('hardwareBackPress', this.onBackAndroid)
    }

    onBackAndroid(){
		if(navigater && navigater.getCurrentRoutes().length>1){
            navigater.pop();
            return true;
		}
        if (this.lastBackPressed && this.lastBackPressed + 2000 >= Date.now()) {
            //最近2秒内按过back键，可以退出应用。
            navigater.pop();
			return false;
        }
        this.lastBackPressed = Date.now();
        ToastAndroid.show('再按一次退出应用', ToastAndroid.SHORT);
        return true;
    }

	goto(index){
		switch (index){
		case openDrawer:
            this.refs[DRAWER_REF].openDrawer();
			break;
		case chargedetail:
            this.props.navigator.push({name: ChargedetailPage});
			break;
		case logout:
            ToastAndroid.show('logout', ToastAndroid.SHORT);
			break;
		case msgcenter:
            this.props.navigator.push({name: MsgCenterPage});
			break;
		case payment:
            this.props.navigator.push({
            	name: PaymentPage,
                params:{
					type:1,
					title:'付款码'
            	}
            });
			break;
		case card:
            this.props.navigator.push({
                name: PaymentPage,
                params:{
                    type:2,
                    title:'银行卡收款'
                }
            });
			break;
		case scanview:
			this.props.navigator.push({name: ScanViewPage});
			break;
		default:
			break;
		};
	}

	request(){
        var url1 = 'http://apis.baidu.com/showapi_open_bus/mobile/find';
        var url2 = 'http://www.pintasty.cn/home/homedynamic';
        var url3 = 'http://facebook.github.io/react-native/movies.json';
        // NetUtil.get(url1, function (ret) {
			// console.log(ret);
        // });

        let params = {'start':'0',limit:'20','isNeedCategory': true, 'lastRefreshTime': '2016-09-25 09:45:12'};
        NetUtil.post(url2, params, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiJVLTliZGJhNjBjMjZiMDQwZGJiMTMwYWRhYWVlN2FkYTg2IiwiZXhwaXJhdGlvblRpbWUiOjE0NzUxMTg4ODU4NTd9.ImbjXRFYDNYFPtK2_Q2jffb2rc5DhTZSZopHG_DAuNU',function (set) {
            //下面的就是请求来的数据
            console.log(set)
        })

        let params2 = {'num':18650794290};
        //get请求,以百度为例,没有参数,没有header
        NetUtil.get(url1,params2,function (set) {
            //下面是请求下来的数据
            console.log(set)
        })


	}

	render() {
        navigater = this.props.navigator;

        let navigationView = (
			<NavigationView
				navigator={this.props.navigator}
				closedrawer={this.close_drawer.bind(this)}/>
        );

		return (
			<DrawerLayoutAndroid
				ref={DRAWER_REF}
				drawerWidth={UIHelper.getScreenWidth()*0.8}
				drawerPosition={DrawerLayoutAndroid.positions.Left}
				renderNavigationView={() => navigationView}>
			<View style={{ flex: 1, backgroundColor: '#FFFFFF',flexDirection:'column'}}>
				<View style={style.boxTop}>

					<View style={{flexDirection:'row',justifyContent:'space-between'}}>
						<TouchableOpacity onPress={() => this.goto(openDrawer)}>
							<Image style={style.drawerIcon}
									source={require('../../../res/drawable/ic_launcher.png')}
							/>
						</TouchableOpacity>

						<TouchableOpacity onPress={()=>this.goto(msgcenter)}>
							<Image style={style.msgIcon}
								   source={require('../../../res/drawable/ic_launcher.png')}
							/>
						</TouchableOpacity>
					</View>

					<View style={{flexDirection:'row', flex:1,justifyContent:'space-around',marginTop:40}}>
						<TouchableOpacity onPress={()=>this.goto(scanview)}>
							<View style={style.actionItem}>
								<Image style={style.actionIcon}
									   source={require('../../../res/drawable/ic_launcher.png')}
								/>
								<Text style={style.actionText} >扫一扫</Text>
							</View>
						</TouchableOpacity>

						<TouchableOpacity onPress={()=>this.goto(payment)}>
							<View style={style.actionItem}>
								<Image style={style.actionIcon}
									   source={require('../../../res/drawable/ic_launcher.png')}
								/>
								<Text style={style.actionText} >付款码</Text>
							</View>
						</TouchableOpacity>

						<TouchableOpacity onPress={()=>this.goto(card)}>
							<View style={style.actionItem}>
								<Image style={style.actionIcon}
									   source={require('../../../res/drawable/ic_launcher.png')}
								/>
								<Text style={style.actionText} >银行卡</Text>
							</View>
						</TouchableOpacity>

					</View>

					<View style={[style.line]}/>
				</View>

				<View style={style.boxCenter}>
					<View style={[style.mainView]}>
						<PercentageCircle
							radius={100}
							percent={80}
							color={"#3498db"}
							borderWidth={8}
							textStyle={{fontSize: 0}}
						/>

						<View style={{flexDirection:'column',marginTop:-150,alignItems:'center'}}>
							<Text style={{fontSize:60,}} >0.00</Text>
							<Text style={{fontSize:15,marginTop:10}} >当日收款总金额</Text>
						</View>

					</View>

					<View style={style.legend}>
						<Image style={style.legendIcon}
							   source={require('../../../res/drawable/ic_launcher.png')}
						/>
						<Text style={[style.actionText,{marginRight:15}]} >日总额</Text>

						<Image style={style.legendIcon}
							   source={require('../../../res/drawable/ic_launcher.png')}
						/>
						<Text style={[style.actionText,{marginRight:15}]} >月总额</Text>
					</View>

				</View>


				<View style={style.boxBottom}>
					<View style={[style.line,{marginTop:0,}]}/>
					<TouchableOpacity onPress={()=>this.goto(chargedetail)}>
						<View style={style.chargedetail}>
							<Text style={{fontSize:15,marginRight:15}} >收款明细</Text>

							<Image style={{width:12, height:20,}}
								   source={require('../../../res/drawable/arrow_r.png')}
							/>
						</View>
					</TouchableOpacity>
				</View>

			</View>
			</DrawerLayoutAndroid>
		);
	}
}

var style = StyleSheet.create({
	boxTop:{
		flex:3.5,
	},
    boxCenter:{
        flex:5.5,
    },
    boxBottom:{
        flex:1,
		flexDirection:'column'
    },

	drawerIcon:{
        width:40,
        height:40,
        marginLeft:15,
		marginTop:15,
	},
	msgIcon:{
        width:30,
        height:30,
        marginRight:15,
        marginTop:17,
	},
    actionIcon:{
        width:50,
        height:50,

    },
	actionText:{
        fontSize:15,
		marginTop:5,
    },
    actionItem:{
        flex:1,
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',
    },
	line:{
		height:1,
		backgroundColor: "#F0F0F0",
        marginTop:43,
	},
	mainView:{
		flexDirection:'column',
		marginTop:50,
		alignItems:'center',
	},
	legend:{
        justifyContent:'flex-end',
		alignItems:'center',
		flexDirection:'row',
		marginTop:90,
	},
    legendIcon:{
		width:20,
		height:20,
		marginRight:10,
	},
    chargedetail:{
		flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
		padding:20,
	},
});

export default HomePage;