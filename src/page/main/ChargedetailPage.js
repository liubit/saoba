/**
 * Created by liubit on 2016/12/7.
 * 收款明细
 */
import React from 'react';
import { TextInput, View, Text, TouchableOpacity,
    StyleSheet,
    ListView,
    Image,
    ToastAndroid,
    DatePickerAndroid,
    DatePickerIOS,
    Platform,
} from 'react-native';

import ToolBarView from '../../widget/ToolBarView';
import DatePickerDialog from '../../widget/DatePickerDialog.ios';
import DatePickerModal from '../../widget/DatePickerModal';
import NetUtil from '../../tools/NetUtil';
import Cancelable from '../../tools/Cancelable';

let self;

class ChargedetailPage extends React.Component {
    constructor(props){
        super(props);
        self = this;
        this.state = {
            dataSource: new ListView.DataSource({
                rowHasChanged: (row1, row2) => row1 !== row2,
            }),
            loaded: false,
            date: new Date(),
            shouldShow : false,

        };
    }

    componentWillUnMount() {
        this.cancelable.cancel();
    }

    componentDidMount() {
        this.request();
    }

    updateList(data){
        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(data.movies),
            loaded: true,
        });
    }

    request(){
        var url1 = 'http://www.pintasty.cn/home/homedynamic';
        var url2 = 'https://raw.githubusercontent.com/facebook/react-native/master/docs/MoviesExample.json';

        // NetUtil.get(url2,'', function (response) {
        //     console.log(response.total);
        //     self.updateList(response);
        // });

        //可取消的请求
        this.cancelable = Cancelable(fetch(url2));
        this.cancelable.promise
            .then((response)=>response.json())
            .then((responseData)=> {
                console.log(responseData.total);
                self.updateList(responseData);
            }).catch((error)=> {
                console.log(error);
            });

        // let params = {'start':'0',limit:'20','isNeedCategory': true, 'lastRefreshTime': '2016-09-25 09:45:12'};
        // NetUtil.post(url1, params, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiJVLTliZGJhNjBjMjZiMDQwZGJiMTMwYWRhYWVlN2FkYTg2IiwiZXhwaXJhdGlvblRpbWUiOjE0NzUxMTg4ODU4NTd9.ImbjXRFYDNYFPtK2_Q2jffb2rc5DhTZSZopHG_DAuNU',
        //     function (response) {
        //         console.log(response);
        //
        // });

        // NetUtil.post(url2,'','',function (response) {
        //     console.log(response);
        // });




    }

    showDatePicker(){
        if(Platform.OS=='ios'){
            this.showDatePicker_iOS();
        }else {
            this.showPicker_android();
        }
    }

    showDatePicker_iOS() {
        let dobDate = this.state.date;

        if(!dobDate || dobDate == null){
            dobDate = new Date();
            this.setState({
                date: dobDate
            });
        }

        //To open the dialog
        this.refs.pickerDialog.open({
            date: dobDate,
            maxDate: new Date() //To restirct future date
        });




    }

    _onDatePicked(date){
        console.log(date);

    }

    async showPicker_android(){
        let stateKey = 'min';
        let options = {date: this.state.maxDate,maxDate:new Date()};

        try {
            var newState = {};
            const {action, year, month, day} = await DatePickerAndroid.open(options);
            if (action === DatePickerAndroid.dismissedAction) {
                newState[stateKey + 'Text'] = 'dismissed';
            } else {
                var date = new Date(year, month, day);
                newState[stateKey + 'Text'] = date.toLocaleDateString();
                newState[stateKey + 'Date'] = date;
                ToastAndroid.show(year+'-'+(month+1)+'-'+day,0);
            }
            this.setState(newState);
        } catch ({code, message}) {
            console.warn(`Error in example '${stateKey}': `, message);
        }
    }

    loading() {
        return (
            <View style={styles.container}>
                <ToolBarView title="收款明细"
                             showPicker={()=>this.showDatePicker()}
                             navigator={this.props.navigator}/>

                <View style={styles.container2}>
                    <Text style={{fontSize:30}}>
                        Loading ...
                    </Text>
                </View>


            </View>
        );
    }

    _renderRow(rowData, sectionID, rowID){
        return (
            <View style={{flexDirection:'row', alignItems:'center',paddingTop:15}}>
                <View style={{flex:3.5,flexDirection:'row'}}>
                    <Text style={{fontSize:16,marginLeft:15,alignSelf:'flex-start'}}>交易成功</Text>

                    <Image
                        style={styles.itemIcon}
                        source={require('../../../res/drawable/ic_launcher.png')} />
                </View>

                <View style={{flex:4, marginLeft:15}}>
                    <Text style={{fontSize:16,lineHeight:30}}>{rowData.title+'\n'+"2016/12/08 18:00"}</Text>
                </View>

                <View style={{flex:3, flexDirection:'row', justifyContent:'flex-end'}}>
                    <Text style={{fontSize:22,marginRight:15}}>$ 55.00</Text>
                </View>

            </View>
        );
    }

    show() {
        this.setState({
            shouldShow : true,
        });
    }
    clickSure(val) {
        console.log("!!!!." + val);
    }
    clickTab(index) {
        console.log("==="+index);
        this.showDatePicker();
    }

    render() {
        if(!this.state.loaded){
            return this.loading();
        }
        return (
            <View style={styles.container}>
                <ToolBarView title="收款明细"
                             toolbarType='2'
                             //showPicker={()=>this.showDatePicker()}
                             showPicker={()=>this.show()}
                             navigator={this.props.navigator}/>

                <DatePickerModal
                    shouldShow = {this.state.shouldShow}
                    clickSure = {this.clickSure.bind(this)}
                    clickTab = {this.clickTab.bind(this)}
                />

                <ListView
                    style={{flex: 1,}}
                    dataSource={this.state.dataSource}
                    renderRow={this._renderRow}
                />

                <DatePickerDialog
                    ref="pickerDialog"
                    onDatePicked={this._onDatePicked.bind(this)} />

            </View>
        );
    }
}

let styles = StyleSheet.create({
    container:{
        flex: 1, backgroundColor: '#FFFFFF' ,
        flexDirection:'column'
    },
    itemIcon:{
        width:25,height:25,
        marginLeft:15,
    },
    container2: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },

});


export default ChargedetailPage;