/**
 * Created by liubit on 2016/12/8.
 * 二维码
 */
import React from 'react';
import { TextInput, View, Text, TouchableOpacity,
    StyleSheet,
    Image,
} from 'react-native';

import ToolBarView from '../../widget/ToolBarView';

let Dimensions = require('Dimensions');
let screenW = Dimensions.get('window').width;

class HandleCardPage extends React.Component{

    render() {
        return (
            <View style={styles.container}>
                <ToolBarView title="操作卡"
                             navigator={this.props.navigator}/>

                <View style={styles.container}>
                        <TouchableOpacity style={styles.row}>
                            <Image style={styles.methodIcon}
                                   source={require('../../../res/drawable/ic_launcher.png')}
                            />
                            <Text style={styles.methodText}>
                                请挥卡
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.row}>
                            <Image style={styles.methodIcon}
                                   source={require('../../../res/drawable/ic_launcher.png')}
                            />
                            <Text style={styles.methodText}>
                                请插卡
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.row}>
                            <Image style={styles.methodIcon}
                                   source={require('../../../res/drawable/ic_launcher.png')}
                            />
                            <Text style={styles.methodText}>
                                请刷卡
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={[styles.row,{flex:1}]}>
                        </TouchableOpacity>

                </View>

            </View>
        )
    }

}

let styles = StyleSheet.create({
    container:{
        flex: 1, backgroundColor: '#FFFFFF' ,
        flexDirection:'column',

    },
    row:{
        flex:2,flexDirection:'row',
        alignItems:'center',
        marginLeft:40,
    },

    methodIcon:{
        width:100,height:100,
    },
    methodText:{
        fontSize:25,
        marginLeft:30,
    }




});

export default HandleCardPage;
