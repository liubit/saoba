/**
 * Created by liubit on 2016/12/7.
 * 消息中心
 */
import React from 'react';
import { TextInput, View, Text, TouchableOpacity,
    StyleSheet,
    ListView,
    Image,
    ToastAndroid,
    Alert,
} from 'react-native';

import ScrollableTabView from 'react-native-scrollable-tab-view';
import ToolBarView from '../../widget/ToolBarView';
import TradeMsgView from '../../widget/TradeMsgView';
import SystemMsgView from '../../widget/SystemMsgView';

let index=0;

class MsgCenterPage extends React.Component {
    clearMsg(){
        if(index==0){
            Alert.alert('清空所有交易通知？','',[
                {text:'取消',},
                {text:'确定',onPress:()=>this.clear()}
            ])
        }else {
            Alert.alert('清空所有系统通知？','',[
                {text:'取消',},
                {text:'确定',onPress:()=>this.clear()}
            ])
        }
    }

    clear(){
        if(index==0){
            ToastAndroid.show('清空所有交易通知成功',ToastAndroid.SHORT)
        }else {
            ToastAndroid.show('清空所有系统通知成功',ToastAndroid.SHORT)
        }

    }

    render() {
        return (
            <View style={styles.container}>
                <ToolBarView title="消息中心"
                             toolbarType='1'
                             clearBtn={()=>this.clearMsg()}
                             navigator={this.props.navigator}/>

                <ScrollableTabView
                    tabBarActiveTextColor="#31C4C1"
                    tabBarTextStyle={{fontSize: 18}}
                    locked={true}
                    onChangeTab={(obj)=>{index=obj.i}}>

                    <TradeMsgView tabLabel='交易通知'/>

                    <SystemMsgView tabLabel='系统通知'/>

                </ScrollableTabView>
            </View>
        );
    }

}

let styles = StyleSheet.create({
    container:{
        flex: 1, backgroundColor: '#FFFFFF' ,
        flexDirection:'column'
    },


});

export default MsgCenterPage;