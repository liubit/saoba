/**
 * Created by liubit on 2016/12/8.
 * 二维码
 */
import React from 'react';
import { TextInput, View, Text, TouchableOpacity,
    StyleSheet,
    Image,
} from 'react-native';

import QRCode from 'react-native-qrcode';
import ToolBarView from '../../widget/ToolBarView';

let Dimensions = require('Dimensions');
let screenW = Dimensions.get('window').width;

class QRCodePage extends React.Component{
    state = {
        text: this.props.text,
    };

    render() {
        return (
            <View style={styles.container}>
                <ToolBarView title="付款码"
                             navigator={this.props.navigator}/>

                <View style={styles.qcCode}>
                    <QRCode
                        value={this.state.text}
                        size={screenW*0.68}
                        bgColor='black'
                        fgColor='white'/>
                </View>

                <View style={{alignItems:'center',flexDirection:'column',}}>
                    <Text style={{fontSize:20}}>
                        请扫描以上付款码
                    </Text>
                </View>

                <View style={{alignItems:'center',flexDirection:'column',}}>
                    <Text style={{fontSize:20,margin:20}}>
                        当前支持
                    </Text>

                    <View style={{flexDirection:'row'}}>
                        <Image style={styles.method}
                               source={require('../../../res/drawable/ic_launcher.png')}
                        />

                        <Image style={[styles.method,{marginLeft:20,}]}
                               source={require('../../../res/drawable/ic_launcher.png')}
                        />
                    </View>
                </View>



            </View>
        )
    }

}

let styles = StyleSheet.create({
    container:{
        flex: 1, backgroundColor: '#FFFFFF' ,
        flexDirection:'column',

    },
    qcCode:{
        flexDirection:'column',
        margin: screenW*0.16,
    },
    method:{
        width: 50,
        height: 50,
    }



});

export default QRCodePage;
