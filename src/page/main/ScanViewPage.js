/**
 * Created by liubit on 2016/12/12.
 * 扫描二维码界面
 */

import React, { Component } from 'react';
import {
    AppRegistry,
    Image,
    StatusBar,
    StyleSheet,
    TouchableOpacity,
    View,
    AlertIOS,
    ToastAndroid,
    Platform,
} from 'react-native';

import Camera from 'react-native-camera';
import ToolBarView from '../../widget/ToolBarView'
let Dimensions = require('Dimensions');
let screenW;
let screenH;

class ScanViewPage extends React.Component {
    constructor(props) {
        super(props);

        screenW = Dimensions.get('window').width;
        screenH = Dimensions.get('window').height;

        this.camera = null;
        this.state = {
            camera: {
                aspect: Camera.constants.Aspect.sretch,
                captureTarget: Camera.constants.CaptureTarget.cameraRoll,
                type: Camera.constants.Type.back,
                orientation: Camera.constants.Orientation.auto,
                flashMode: Camera.constants.FlashMode.auto,
            },
            isRecording: false
        };

    }


    onBarCodeRead=(e)=>{
        console.log(e.data)
        if(Platform.OS=='android'){
            ToastAndroid.show(e.data,ToastAndroid.SHORT);
        }
    }
    render() {
        return (
            <View style={styles.container}>

                <ToolBarView
                    title="扫一扫"
                    navigator={this.props.navigator}
                    />

                <Camera
                    ref={(cam) => {
                        this.camera = cam;
                    }}
                    style={styles.preview}
                    aspect={this.state.camera.aspect}
                    captureTarget={this.state.camera.captureTarget}
                    type={this.state.camera.type}
                    flashMode={this.state.camera.flashMode}
                    defaultTouchToFocus
                    mirrorImage={false}
                    onBarCodeRead={this.onBarCodeRead}
                />

                <Image style={[styles.overlay,
                        {width:screenW*0.7,height:screenW*0.7,left:screenW*0.15,top:screenH*0.3}]}
                    source={require('../../../res/drawable/scanning.png')}
                    resizeMode={'stretch'}
                />


            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    overlay: {
        position: 'absolute',
        flexDirection: 'column',
        alignItems: 'center',
        top:160,
    },
    topOverlay: {
        top: 0,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    bottomOverlay: {
        bottom: 0,
        backgroundColor: 'rgba(0,0,0,0.4)',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    captureButton: {
        padding: 15,
        backgroundColor: 'white',
        borderRadius: 40,
    },
    typeButton: {
        padding: 5,
    },
    flashButton: {
        padding: 5,
    },
    buttonsSpace: {
        width: 10,
    },
});

export default ScanViewPage;
