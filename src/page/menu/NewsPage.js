/**
 * Created by liubit on 2016/12/8.
 * 资讯
 */
import React from 'react';
import { TextInput, View, Text, TouchableOpacity,
    StyleSheet,
    ToastAndroid,
    ListView,
} from 'react-native';

import ToolBarView from '../../widget/ToolBarView';
import WebviewPage from './WebViewPage';

class NewsPage extends React.Component{
    constructor(props){
        super(props);
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(this._genRows()),
        };
    }

    _genRows(){
        const dataBlob = [];
        let news;
        for(let i = 1 ; i< 30 ; i ++ ){
            news= ["产品自寻标题"+i, '产品资讯副标题或内容节选'+i, '12/'+i];
            dataBlob.push(news);
        }
        return dataBlob;
    }

    newsItem(index){
        this.props.navigator.push({
            name: WebviewPage,
            params:{
                url: 'https://www.baidu.com/',
            }
        });
    }

    _renderRow(rowData, sectionID, rowID){
        return (
            <TouchableOpacity onPress={()=>this.newsItem(rowID)}>
                <View style={{flexDirection:'column',paddingLeft:15,paddingTop:15,paddingRight:15}}>
                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                        <Text style={{fontSize:20,}}>{rowData[0]}</Text>

                        <Text style={{fontSize:16,color:'#e0e1d5' }}>{rowData[2]}</Text>
                    </View>

                    <Text style={{fontSize:16,marginTop:5,color:'#e0e1d5' }}>{rowData[1]}</Text>

                    <View style={styles.line}/>
                </View>
            </TouchableOpacity>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <ToolBarView title='推广'
                             navigator={this.props.navigator}/>

                <View style={styles.adBox}>
                    <Text style={{fontSize:20}}>设备图文广告</Text>
                </View>

                <View style={{flex:7}}>
                    <ListView style={{flex:1}}
                              dataSource={this.state.dataSource}
                              renderRow={this._renderRow.bind(this)}
                    />
                </View>

            </View>
        )
    }

}

let styles = StyleSheet.create({
    container:{
        flex: 1, backgroundColor: '#FFFFFF' ,
        flexDirection:'column',
    },
    adBox:{
        flex:3,backgroundColor:'#e3f0e7',
        justifyContent:'center',
        alignItems:'center',
    },
    line:{
        height:1,
        backgroundColor: "#F0F0F0",
        marginTop: 15,
    },



});

export default NewsPage;
