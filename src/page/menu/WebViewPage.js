/**
 * Created by liubit on 2016/12/8.
 * 显示网页
 */
import React from 'react';
import { TextInput, View, Text, TouchableOpacity,
    StyleSheet,
    ToastAndroid,
    ListView,
    WebView,
} from 'react-native';

import ToolBarView from '../../widget/ToolBarView';

export default class WebViewPage extends React.Component{

    render() {

        return (
            <View style={styles.container}>
                <ToolBarView title='推广'
                             navigator={this.props.navigator}/>

                <WebView
                    style={{flex:1}}
                    source={{uri: this.props.url}}
                    startInLoadingState={true}
                    domStorageEnabled={true}
                    javaScriptEnabled={true}
                />

            </View>
        )
    }

}

let styles = StyleSheet.create({
    container:{
        flex: 1, backgroundColor: '#FFFFFF' ,
        flexDirection:'column',
    },


});

