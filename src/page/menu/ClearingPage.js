/**
 * Created by liubit on 2016/12/8.
 * 帐号管理
 */
import React from 'react';
import { TextInput, View, Text, TouchableOpacity,
    StyleSheet,
    ToastAndroid,
    Switch,
    Image,
} from 'react-native';

import ToolBarView from '../../widget/ToolBarView';

class ClearingPage extends React.Component{
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {
            status: false
        };
    }

    switchOnorOff(value){
        ToastAndroid.show('status:'+value,ToastAndroid.SHORT);
        this.setState({status: value})
    }

    render() {
        return (
            <View style={styles.container}>
                <ToolBarView title='结算设置'
                             navigator={this.props.navigator}/>

                <View style={styles.container}>
                    <View style={{flexDirection:'row'}}>
                        <View style={styles.item_left}>
                            <View style={{flexDirection:'row'}}>
                                <Text style={{fontSize:20,}}>T+1结算</Text>
                                <Text style={styles.handling_cost}>手续费：0</Text>
                            </View>

                            <Text style={{fontSize:18,marginTop:10,}}>工作日收款成功后自动结算到账</Text>
                        </View>

                        <View style={styles.item_right}>
                            <Switch
                                value={this.state.status}
                                onValueChange={(value)=>this.switchOnorOff(value)}/>
                        </View>

                    </View>

                    <View style={styles.line}/>

                    <View style={{flexDirection:'row'}}>
                        <View style={styles.item_left}>
                            <View style={{flexDirection:'row'}}>
                                <Text style={{fontSize:20,}}>D+1结算</Text>
                                <Text style={styles.handling_cost}>手续费：结算资金*0.15%</Text>
                            </View>

                            <Text style={{fontSize:18,marginTop:10,}}>节假日收款成功后自动结算到账</Text>
                        </View>

                        <View style={styles.item_right}>
                            <Switch value={this.state.status}
                                    onValueChange={(value)=>this.switchOnorOff(value)}/>
                        </View>

                    </View>

                    <View style={styles.line}/>

                    <View style={{flexDirection:'row'}}>
                        <View style={styles.item_left}>
                            <View style={{flexDirection:'row'}}>
                                <Text style={{fontSize:20,}}>T+S结算</Text>
                                <Text style={styles.handling_cost}>手续费：结算资金*TS指定费率</Text>
                            </View>

                            <Text style={{fontSize:18,marginTop:10,}}>收款成功后自动结算，实时到帐</Text>
                        </View>

                        <View style={styles.item_right}>
                            <Switch value={this.state.status}
                                    onValueChange={(value)=>this.switchOnorOff(value)}/>
                        </View>

                    </View>

                    <View style={styles.line}/>

                    <View style={{flexDirection:'column',padding:15}}>
                        <View style={{flexDirection:'row',alignItems:'center'}}>
                            <Image style={{width:30,height:30}}
                                   source={require('../../../res/drawable/ic_launcher.png')}/>
                            <Text style={{fontSize:18,marginLeft:15}}>结算方式设置须知</Text>
                        </View>

                        <Text style={{fontSize:18,marginTop:10}}>
                            1、因手续费会受银行出款收费浮动影响，手续费收取以实际结算为准；{'\n'}{'\n'}
                            2、因结算受银行出款限制，结算方式以实际出款为准；{'\n'}{'\n'}
                            3、因D+1出款受T+1业务限制，开通D+1前请先开通T+1；关闭T+1时，D+1也将被关闭。
                        </Text>
                    </View>

                </View>
            </View>
        )
    }

}

let styles = StyleSheet.create({
    container:{
        flex: 1, backgroundColor: '#FFFFFF' ,
        flexDirection:'column',
    },
    item_left:{
        flex:8,
        flexDirection:'column',
        padding:15,
    },
    item_right:{
        flex:2,
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'flex-end',
        marginRight:8,
    },
    line:{
        height:1.5,
        backgroundColor: "#F0F0F0",
    },
    handling_cost:{
        fontSize:17,marginLeft:10,
        borderRadius:3,
        borderWidth:1,
        borderColor:'#f0f0f0',
        paddingTop:2,
        paddingBottom:2,
        paddingLeft:4,
        paddingRight:4,
    }


});

export default ClearingPage;
