/**
 * Created by liubit on 2016/12/8.
 * 帐号管理
 */
import React from 'react';
import { TextInput, View, Text, TouchableOpacity,
    StyleSheet,
    ToastAndroid,
    ListView,
} from 'react-native';

import ToolBarView from '../../widget/ToolBarView';

class AccountManagePage extends React.Component{

    constructor(props){
        super(props);
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(this._genRows()),
        };
    }

    _genRows(){
        const dataBlob = [];
        let account0 = ["中国建设银行", '1111 1111 1111 1111'];
        let account1 = ["中国招商银行", '2222 2222 2222 2222'];
        let account2 = ["中国中信银行", '3333 3333 3333 3333'];
        dataBlob.push(account0);
        dataBlob.push(account1);
        dataBlob.push(account2);
        return dataBlob;
    }

    _renderRow(rowData, sectionID, rowID){
        return (
            <View style={styles.card}>
                <Text style={{fontSize:16 }}>{rowData[0]}</Text>

                <Text style={{fontSize:16, marginTop:15 }}>{rowData[1]}</Text>
            </View>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <ToolBarView title='帐号管理'
                             navigator={this.props.navigator}/>

                <ListView style={{flex:1}}
                    dataSource={this.state.dataSource}
                    renderRow={this._renderRow}
                />

            </View>
        )
    }

}

let styles = StyleSheet.create({
    container:{
        flex: 1, backgroundColor: '#FFFFFF' ,
        flexDirection:'column',

    },
    card:{
        backgroundColor: '#bee1d3' ,
        flexDirection:'column',
        padding:15,
        marginTop:10,
        marginLeft:10,
        marginRight:10,
        borderColor:'#f0f0f0',
        borderWidth:1.5,
        borderRadius:5,
    }



});

export default AccountManagePage;
