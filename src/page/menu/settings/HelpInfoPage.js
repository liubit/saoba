/**
 * Created by liubit on 2016/12/7.
 * 设置
 */
import React from 'react';
import { View, Text, TouchableOpacity,
    StyleSheet,
    Image,
} from 'react-native';


import ToolBarView from '../../../widget/ToolBarView';


class HelpInfoPage extends React.Component{

    render() {
        return (
            <View style={styles.container}>
                <ToolBarView title='帮助信息'
                             navigator={this.props.navigator}/>

                <View style={styles.line}/>

            </View>
        )
    }

}

let styles = StyleSheet.create({
    container:{
        flex: 1, backgroundColor: '#FFFFFF' ,
        flexDirection:'column',

    },
    line:{
        height:1,
        backgroundColor: "#F0F0F0",
    },



});

export default HelpInfoPage;
