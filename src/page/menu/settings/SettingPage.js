/**
 * Created by liubit on 2016/12/7.
 * 设置
 */
import React from 'react';
import { View, Text, TouchableOpacity,
    StyleSheet,
    Image,
} from 'react-native';


import ToolBarView from '../../../widget/ToolBarView';
import VersionInfoPage from './VersionInfoPage';
import BindDevicePage from './BindDevicePage';
import HelpInfoPage from './HelpInfoPage';


class SettingPage extends React.Component{
    goto(index) {
        switch (index) {
        case 1:
            this.props.navigator.push({name:VersionInfoPage});
            break;
        case 2:
            this.props.navigator.push({name:BindDevicePage});
            break;
        case 3:
            this.props.navigator.push({name:HelpInfoPage});
            break;
        default:
            break;
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <ToolBarView title='设置'
                             navigator={this.props.navigator}/>

                <View style={styles.item_box}>
                    <TouchableOpacity style={styles.item}
                                      onPress={()=>this.goto(1)}>
                        <Text style={{fontSize:18,marginRight:15}} >版本信息</Text>

                        <Image style={{width:12, height:20,}}
                               source={require('../../../../res/drawable/arrow_r.png')}
                        />
                    </TouchableOpacity>
                </View>

                <View style={styles.line}/>

                <View style={styles.item_box}>
                    <TouchableOpacity style={styles.item}
                                      onPress={()=>this.goto(2)}>
                        <Text style={{fontSize:18,marginRight:15}} >绑定设备</Text>

                        <Image style={{width:12, height:20,}}
                               source={require('../../../../res/drawable/arrow_r.png')}
                        />
                    </TouchableOpacity>
                </View>

                <View style={styles.line}/>

                <View style={styles.item_box}>
                    <TouchableOpacity style={styles.item}
                                      onPress={()=>this.goto(3)}>
                        <Text style={{fontSize:18,marginRight:15}} >帮助信息</Text>

                        <Image style={{width:12, height:20,}}
                               source={require('../../../../res/drawable/arrow_r.png')}
                        />
                    </TouchableOpacity>
                </View>

                <View style={styles.line}/>

            </View>
        )
    }

}

let styles = StyleSheet.create({
    container:{
        flex: 1, backgroundColor: '#FFFFFF' ,
        flexDirection:'column',

    },
    item_box:{
        height:60,
    },
    item:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        padding:20,
    },
    line:{
        height:1,
        backgroundColor: "#F0F0F0",
    },



});

export default SettingPage;
