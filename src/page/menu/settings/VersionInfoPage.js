/**
 * Created by liubit on 2016/12/7.
 * 设置
 */
import React from 'react';
import { View, Text, TouchableOpacity,
    StyleSheet,
    Image,
} from 'react-native';


import ToolBarView from '../../../widget/ToolBarView';


class VersionInfoPage extends React.Component{

    render() {
        return (
            <View style={styles.container}>
                <ToolBarView title='版本信息'
                             navigator={this.props.navigator}/>

                <View style={styles.item_box}>
                    <Text style={{fontSize:18}} >软件版本</Text>
                    <Text style={{fontSize:18,marginTop:13}} >v1.0.2</Text>
                </View>

                <View style={styles.line}/>

                <View style={styles.item_box}>
                    <Text style={{fontSize:18}} >SDK版本</Text>
                    <Text style={{fontSize:18,marginTop:13}} >2.0.4</Text>
                </View>

                <View style={styles.line}/>


            </View>
        )
    }

}

let styles = StyleSheet.create({
    container:{
        flex: 1, backgroundColor: '#FFFFFF' ,
        flexDirection:'column',

    },
    item_box:{
        padding:20,
    },
    line:{
        height:1,
        backgroundColor: "#F0F0F0",
    },



});

export default VersionInfoPage;
