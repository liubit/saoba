/**
 * Created by liubit on 2016/12/7.
 */
'use strict'
import React from 'react';
import { StyleSheet,
    AppRegistry,
    Component,
    View,
    Text,
    Image,
    TouchableOpacity,
    Platform,
    ToastAndroid,
}from 'react-native';

import SegmentTabView from './SegmentTabView';
import {UIHelper} from '../tools/UIHelper';

const MSGCENTER=1;
const CHARGE=2;

let toolbarPaddingTop = 0;
let toolbarPaddingBottom = 0;
let toolbarHeight = 60;
let selected = 0;

var ToolBarView = React.createClass({

    componentWillMount(){
        if (Platform.OS === 'ios') {
            toolbarPaddingTop = 35;
            toolbarPaddingBottom = 10;
            toolbarHeight = 80;
        };
        this.state = {
            selected: 0
        }

    },

    onPressTab(index){
        this.setState({selected: index});

    },

    rightMenu(){
        if(this.props.toolbarType==MSGCENTER){
            return(
                <View style={styles.rightMenu}>
                    <TouchableOpacity onPress={()=>this.props.clearBtn()}>
                        <Text style={{fontSize:18,marginRight:15,}}>清空</Text>
                    </TouchableOpacity>
                </View>
            );
        }else if(this.props.toolbarType==CHARGE){
            return(
                <View style={styles.rightMenu}>
                    <SegmentTabView style={{marginRight:15}}
                        data={['日', '月']}
                        selected={this.state.selected}
                        onPress={ index => this.onPressTab(index)}
                    />

                    <TouchableOpacity onPress={()=>this.props.showPicker()}>
                        <Image
                            style={[styles.rightIcon,{width:25,height:25,}]}
                            source={require('../../res/drawable/ic_launcher.png')}/>
                    </TouchableOpacity>
                </View>
            );
        }else {
            return null;
        }
    },

    render(){
        return(
            <View style={[styles.toolbar,
                {height:toolbarHeight,paddingTop:toolbarPaddingTop,paddingBottom:toolbarPaddingBottom,}]}>

                <View style={{flex:7,flexDirection:'row',}}>
                    <TouchableOpacity onPress={this.props.navigator.pop}>
                        <Image
                            style={styles.backIcon}
                            source={require('../../res/drawable/icon_back.png')} />
                    </TouchableOpacity>

                    <Text style={styles.title}>{this.props.title}</Text>
                </View>

                {this.rightMenu()}

            </View>

        )


    }
});

let styles = StyleSheet.create({
    toolbar:{
        backgroundColor: '#f0f0f0',
        flexDirection:'row',
        justifyContent:'flex-start',
        alignItems:'center',
    },
    backIcon:{
        width:25,height:25,
        marginLeft:15,
    },
    title:{
        fontSize:23,
        marginLeft:15,
    },
    rightIcon:{
        width:25,height:25,
        marginRight:15,

    },
    rightMenu:{
        flex:3,flexDirection:'row',justifyContent:'flex-end',
    },


});

module.exports = ToolBarView;