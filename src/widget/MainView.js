/**
 * Created by liubit on 2016/12/06.
 */
'use strict'
import React from 'react';
import { StyleSheet,
    StyleSheet,
    AppRegistry,
    Component,
    View,
    Text,
    Image,
    TouchableOpacity,
}from 'react-native';

import PercentageCircle from 'react-native-percentage-circle';
let DRAWER_REF = 'drawer';

let MainView = React.createClass({
    render(){
        return(
            <View style={{ flex: 1, backgroundColor: '#FFFFFF',flexDirection:'column'}}>
                <View style={stylesMain.boxTop}>

                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                        <TouchableOpacity onPress={() => this.refs[DRAWER_REF].openDrawer()}>
                            <Image style={stylesMain.drawerIcon}
                                   source={require('./drawable/ic_launcher.png')}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity>
                            <Image style={stylesMain.msgIcon}
                                   source={require('./drawable/ic_launcher.png')}
                            />
                        </TouchableOpacity>
                    </View>

                    <View style={{flexDirection:'row', justifyContent:'space-around',marginTop:40}}>
                        <TouchableOpacity>
                            <View style={stylesMain.actionItem}>
                                <Image style={stylesMain.actionIcon}
                                       source={require('./drawable/ic_launcher.png')}
                                />
                                <Text style={stylesMain.actionText} >扫一扫</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity>
                            <View style={stylesMain.actionItem}>
                                <Image style={stylesMain.actionIcon}
                                       source={require('./drawable/ic_launcher.png')}
                                />
                                <Text style={stylesMain.actionText} >一码付</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity>
                            <View style={stylesMain.actionItem}>
                                <Image style={stylesMain.actionIcon}
                                       source={require('./drawable/ic_launcher.png')}
                                />
                                <Text style={stylesMain.actionText} >银行卡</Text>
                            </View>
                        </TouchableOpacity>

                    </View>

                    <View style={[stylesMain.line]}/>

                </View>

                <View style={stylesMain.boxCenter}>

                    <View style={[stylesMain.mainView]}>
                        <PercentageCircle
                            radius={100}
                            percent={80}
                            color={"#3498db"}
                            borderWidth={8}
                            textStyle={{fontSize: 0}}
                        />


                    </View>

                    <View style={stylesMain.legend}>
                        <Image style={stylesMain.legendIcon}
                               source={require('./drawable/ic_launcher.png')}
                        />
                        <Text style={[stylesMain.actionText,{marginRight:15}]} >日总额</Text>

                        <Image style={stylesMain.legendIcon}
                               source={require('./drawable/ic_launcher.png')}
                        />
                        <Text style={[stylesMain.actionText,{marginRight:15}]} >月总额</Text>
                    </View>

                    <View style={[stylesMain.line,{marginTop:30,}]}/>
                </View>

                <View style={stylesMain.boxBottom}>

                    <View style={stylesMain.chargedetail}>
                        <Text style={{fontSize:15,marginRight:15}} >收款明细</Text>

                        <Image style={{width:20, height:20,}}
                               source={require('./drawable/ic_launcher.png')}
                        />
                    </View>
                </View>

            </View>

        )
    }
});

let stylesMain = StyleSheet.create({
    boxTop:{
        flex:3.5,
    },
    boxCenter:{
        flex:5.5,
    },
    boxBottom:{
        flex:1,
    },

    drawerIcon:{
        width:40,
        height:40,
        marginLeft:15,
        marginTop:15,
    },
    msgIcon:{
        width:30,
        height:30,
        marginRight:15,
        marginTop:17,
    },
    actionIcon:{
        width:50,
        height:50,

    },
    actionText:{
        fontSize:15,
        marginTop:5,
    },
    actionItem:{
        flex:1,
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',
    },
    line:{
        height:1,
        backgroundColor: "#F0F0F0",
        marginTop:43,
    },
    mainView:{
        marginTop:40,
        alignItems:'center',
    },
    legend:{
        justifyContent:'flex-end',
        alignItems:'center',
        flexDirection:'row',
        marginTop:40,
    },
    legendIcon:{
        width:20,
        height:20,
        marginRight:10,
    },
    chargedetail:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        padding:20,
    },
});

module.exports = MainView;