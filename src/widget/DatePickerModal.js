import React, {component} from 'react';
import {
    Modal,
    View,
    TouchableOpacity,
    Text,
    StyleSheet,
    Platform,
}from 'react-native';

import UIHelper from '../tools/UIHelper';
var Dimensions = require('Dimensions');

let toolbarHeight = 60;

export  default class DatePickerModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            shouldShow :false,
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            // 就在这里,nextProps暴露了一个shouldShow属性
            shouldShow : nextProps.shouldShow,
        });
    }
    render() {
        if (Platform.OS === 'ios') {
            toolbarHeight = 80;
        };

        return(
            <Modal
                animationType = {'slide'}
                transparent = {true}
                visible = {this.state.shouldShow}
                onRequestClose = {() => {
                       console.log("<== modal has been closed.");
                    }}>

                <View style = {[styles.modalView,{marginTop:toolbarHeight}]}>
                    <Text style={styles.text}>选择时间</Text>

                    <View style={styles.line}/>

                    <View style={styles.item}>
                        <Text style={styles.text}>起始时间</Text>

                        <TouchableOpacity onPress = {() => this.clickTab(0)}>
                            <Text style={styles.text}>2016/12/16</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.line}/>

                    <View style={styles.item}>
                        <Text style={styles.text}>结束时间</Text>

                        <TouchableOpacity onPress = {() => this.clickTab(1)}>
                            <Text style={styles.text}>2016/12/16</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.line}/>

                    <View style={styles.item}>
                        <Text style={styles.text}>交易状态</Text>

                        <TouchableOpacity onPress = {() => this.clickTab(2)}>
                            <Text style={styles.text}>交易成功</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.line}/>

                    <View style={{flex:1,flexDirection:'column',justifyContent:'center',alignItems:'center',}}>
                        <TouchableOpacity style={styles.commitBtn}
                            onPress = {() => this.clickSure()}>
                            <Text style={{fontSize:20}}>确认</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </Modal>
        );
    }
    /**
     * 设置modal显示还是隐藏
     * @param visible
     */
    setModalVisible(visible) {
        this.setState({
            shouldShow : visible
        });
    }
    /**
     * 取消按钮点击事件
     */
    clickTab(index) {
        //this.setModalVisible(false);
        // 类似delegate,将clickCancel这个接口暴露出去, 让受委托方处理
        this.props.clickTab(index);
    }

    clickSure() {
        this.setModalVisible(false);
        // 在暴露该接口时候, 也可以传递一些参数
        this.props.clickSure(1);
    }}

const styles = StyleSheet.create({
    modalView: {
        flexDirection: 'column',
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height * 0.45,
        backgroundColor: '#f0f0f0',
        borderRadius: 0,
    },
    text:{
        fontSize:17,
        padding:15,
    },
    line:{
        height:1,
        backgroundColor: "#ccc",
    },
    item:{
        flexDirection:'row',
        justifyContent:'space-between'
    },
    commitBtn:{
        borderRadius: 5,
        borderWidth:1,
        borderColor: '#a0a0a0',
        padding:15,
        width:200,
        alignItems:'center',

    }


});