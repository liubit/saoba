/**
 * Created by liubit on 2016/12/6.
 */
'use strict'
import React from 'react';
import { StyleSheet,
    AppRegistry,
    Component,
    View,
    Text,
    Image,
    TouchableOpacity,
    ToastAndroid,
    Platform,
}from 'react-native';

import SettingPage from '../page/menu/settings/SettingPage';
import ChargedetailPage from '../page/main/ChargedetailPage';
import LoginPage from '../page/login/LoginPage';
import AccountManagePage from '../page/menu/AccountManagePage';
import ClearingPage from '../page/menu/ClearingPage';
import NewsPage from '../page/menu/NewsPage';
let chargedetail = 1;
let account = 2;
let clearing = 3;
let news = 4;
let setting = 5;
let logout = 6;
let login = 7;
let paddingTop = 30;

var NavigationView = React.createClass({
    componentWillMount(){
        if (Platform.OS === 'ios') {
            paddingTop = 60;
        }
    },

    goto(index){
        switch (index) {
        case chargedetail:
            this.props.closedrawer(false);
            this.props.navigator.push({name:ChargedetailPage});
            break;
        case account:
            this.props.closedrawer(false);
            this.props.navigator.push({name:AccountManagePage});
            break;
        case clearing:
            this.props.closedrawer(false);
            this.props.navigator.push({name:ClearingPage});
            break;
        case news:
            this.props.closedrawer(false);
            this.props.navigator.push({name:NewsPage});
            break;
        case setting:
            this.props.closedrawer(false);
            this.props.navigator.push({name:SettingPage});
            break;
        case logout:
            ToastAndroid.show('logout', ToastAndroid.SHORT);
            break;
        case login:
            this.props.closedrawer(false);
            this.props.navigator.push({name:LoginPage});
            break;
        default:
            break;
        }
    },

    render(){
        return(
            <View style={{flex: 1,backgroundColor: '#fff'}}>
                <View style={{flexDirection:'column', marginTop:paddingTop,
                    justifyContent:'center',
                    alignItems:'center'}}>
                    <TouchableOpacity onPress={()=>this.goto(login)} >
                        <Image
                            style={{width:80,height:80,}}
                            source={require('../../res/drawable/ic_launcher.png')}
                            />
                    </TouchableOpacity>
                    <Text style={{fontSize:25,marginTop:10}}>桥亭</Text>
                    <Text style={{fontSize:18,marginTop:5}}>18300000000</Text>
                </View>

                <TouchableOpacity style={[styles.item_box,{marginTop:40,}]}
                                  onPress={()=>this.goto(chargedetail)}>
                    <Image
                        style={styles.item_icon}
                        source={require('../../res/drawable/ic_launcher.png')} />
                    <Text style={styles.item_text}>收款明细</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.item_box}
                                  onPress={()=>this.goto(account)}>
                    <Image
                        style={styles.item_icon}
                        source={require('../../res/drawable/ic_launcher.png')} />
                    <Text style={styles.item_text}>账号管理</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.item_box}
                                  onPress={()=>this.goto(clearing)}>
                    <Image
                        style={styles.item_icon}
                        source={require('../../res/drawable/ic_launcher.png')} />
                    <Text style={styles.item_text}>结算设置</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.item_box}
                                  onPress={()=>this.goto(news)}>
                    <Image
                        style={styles.item_icon}
                        source={require('../../res/drawable/ic_launcher.png')} />
                    <Text style={styles.item_text}>产品资讯</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.item_box}
                                  onPress={()=>this.goto(setting)}>
                    <Image
                        style={styles.item_icon}
                        source={require('../../res/drawable/ic_launcher.png')} />
                    <Text style={styles.item_text}>设置</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.logoutBox}
                                  onPress={()=>this.goto(logout)}>
                        <Text style={styles.logout_text}>退出帐号</Text>
                </TouchableOpacity>

            </View>

        )
    }
});

let styles = StyleSheet.create({
    style_drawerItem:{
        flexDirection:'row',
        marginTop:10,
        marginLeft:15,

    },
    item_box:{
        flexDirection:'row',
        marginTop:20,
        marginLeft:30,
        alignItems:'center',
    },
    item_icon:{
        width:30,height:30,
    },
    item_text:{
        fontSize:18,
        marginLeft:20,
    },
    logoutBox:{
        flex:1, justifyContent:'flex-end',alignItems:'center',flexDirection:'column',
    },
    logout_text:{
        fontSize:23,
        marginBottom:20,
        marginTop:20,
    },
});

module.exports = NavigationView;
