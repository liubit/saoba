/**
 * Created by liubit on 2016/12/7.
 * 系统通知
 */
'use strict'
import React from 'react';
import { StyleSheet,
    AppRegistry,
    Component,
    View,
    Text,
    Image,
    ListView,
    TouchableOpacity,
    ToastAndroid,
}from 'react-native';

import SwipeListView from './SwipeListView';
import SwipeRow from './SwipeRow';

class SystemMsgView extends React.Component{
    constructor(props){
        super(props);
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(this._getRows()),
        };

    }

    _getRows(){
        const dataBlob = [];
        for(let i = 1 ; i< 30 ; i ++ ){
            dataBlob.push('2016/12/'+i);
        }
        return dataBlob;
    }

    _renderRow(data, secId, rowId, rowMap){
        rowId++;
        return(
            <View style={styles.containter}>
                <Text style={{fontSize:20,marginLeft:10}}>{data}</Text>

                <Text style={{fontSize:18,marginLeft:10,marginTop:5}}>{'系统通知 '+rowId+'\n容器默认存在两根轴：主轴（main axis）和交叉轴（cross axis）。主轴的开始位置（与边框的交叉点）叫做main start，结束位置叫做main end；交叉轴的开始位置叫做cross start，结束位置叫做cross end'}</Text>
            </View>
        );
    }

    deleteRow(data, secId, rowId, rowMap){
        rowMap[`${secId}${rowId}`].closeRow();

    }

    _renderHiddenRow(data, secId, rowId, rowMap){
        return(
            <View style={styles.rowBack}>
                <TouchableOpacity
                    style={[styles.backRightBtn, styles.backRightBtnRight] }
                    onPress={ () =>this.deleteRow(data, secId, rowId, rowMap)}>
                    <Text style={{color: '#FFF'}}>Delete</Text>
                </TouchableOpacity>
            </View>
        );
    }

    render(){
        return(
            <View style={{flex: 1,backgroundColor: '#fff'}}>

                <SwipeListView
                    style={{flex:1}}
                    dataSource={this.state.dataSource}
                    disableRightSwipe={true}
                    rightOpenValue={-75}
                    renderRow={ (data, secId, rowId, rowMap) => this._renderRow(data, secId, rowId, rowMap)}
                    renderHiddenRow={ (data, secId, rowId, rowMap) => this._renderHiddenRow(data, secId, rowId, rowMap)}
                />

            </View>

        )
    }
};

let styles = StyleSheet.create({
    containter:{
        flexDirection:'column',
        borderColor:'#f0f0f0',
        borderWidth:1.5,
        marginLeft:10,
        marginRight:10,
        marginTop:10,
        paddingBottom:5,
        paddingTop:5,
        backgroundColor: 'white',
        flex: 1,
    },
    msg:{
        fontSize:18,marginLeft:10,marginTop:5,
        marginRight:10,
    },
    rowBack: {
        alignItems: 'center',
        backgroundColor: '#8BC645',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        paddingLeft: 15,

        borderColor:'#f0f0f0',
        borderWidth:2,
        marginLeft:10,
        marginRight:10,
        marginTop:10,
        paddingBottom:5,
    },
    backRightBtn: {
        alignItems: 'center',
        bottom: 0,
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        width: 75
    },
    backRightBtnRight: {
        backgroundColor: '#8BC645',
        right: 0
    },

})

export default SystemMsgView;