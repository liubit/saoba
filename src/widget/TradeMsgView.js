/**
 * Created by liubit on 2016/12/7.
 * 交易通知
 */
'use strict'
import React from 'react';
import { StyleSheet,
    AppRegistry,
    Component,
    View,
    Text,
    Image,
    ListView,
    TouchableOpacity,
}from 'react-native';

class TradeMsgView extends React.Component{
    constructor(props){
        super(props);
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(this._getRows()),
        };
    }

    _getRows(){
        const dataBlob = [];
        for(let i = 0 ; i< 30 ; i ++ ){
            dataBlob.push(888.05+i);
        }
        return dataBlob;
    }

    _renderRow(rowData, sectionID, rowID){
        return (
            <View style={styles.containter}>
                <View style={{flexDirection:'row',}}>
                    <Text style={{fontSize:18,marginLeft:15,paddingTop:5}}>交易成功</Text>
                </View>

                <View style={{flexDirection:'row',}}>
                    <Text style={{fontSize:18,marginLeft:15,color:'#f0f0ff'}}>2016/12/01 12:00:30</Text>
                </View>

                <View style={{alignItems:'center',paddingTop:15,paddingBottom:15}}>
                    <Text style={{fontSize:30,marginRight:15}}>{'$ '+rowData}</Text>
                </View>

                <View style={{flexDirection:'row',}}>
                    <View style={styles.item_left}>
                        <Text style={{fontSize:18,}}>交易时间：</Text>
                    </View>
                    <View style={styles.item_right}>
                        <Text style={{fontSize:18,}}>2016/12/07 12:00:30</Text>
                    </View>
                </View>

                <View style={{flexDirection:'row',}}>
                    <View style={styles.item_left}>
                        <Text style={{fontSize:18,}}>收款卡号：</Text>
                    </View>
                    <View style={styles.item_right}>
                        <Text style={{fontSize:18,}}>尾号1234</Text>
                    </View>
                </View>

                <View style={{flexDirection:'row',}}>
                    <View style={styles.item_left}>
                        <Text style={{fontSize:18,}}>交易金额：</Text>
                    </View>
                    <View style={styles.item_right}>
                        <Text style={{fontSize:18,}}>550.00元</Text>
                    </View>
                </View>

            </View>
        );
    }

    render(){
        return(
            <View style={{flex: 1,backgroundColor: '#fff'}}>

                <ListView
                    style={{flex:1}}
                    dataSource={this.state.dataSource}
                    renderRow={this._renderRow}
                />

            </View>

        )
    }
}

let styles = StyleSheet.create({
    containter:{
        flexDirection:'column',
        borderColor:'#f0f0f0',
        borderWidth:1.5,
        marginLeft:10,
        marginRight:10,
        marginTop:10,
        paddingBottom:8
    },
    item_left:{
        flex:3,
        flexDirection:'row',
        justifyContent:'flex-end',
    },
    item_right:{
        flex:7,
    },

});

export default TradeMsgView;