/**
 * Created by liubit on 2016/3/1.
 */
'use strict';
import React from 'react';
import {
    ToastAndroid,
    Platform,
}from 'react-native';

var Dimensions = require('Dimensions');
var PixelRatio = require('PixelRatio');

let toolbarHeight_android = 60;
let toolbarPaddingTop_android = 0;
let toolbarPaddingBottom_android = 0;

let toolbarHeight_ios = 80;
let toolbarPaddingTop_ios = 35;
let toolbarPaddingBottom_ios = 10;

export function ToastShort(content){
    ToastAndroid.show(new String(content), ToastAndroid.SHORT);
}

export function ToastLong(content){
    ToastAndroid.show(new String(content), ToastAndroid.LONG);
}

export function getScreenWidth(){
    return Dimensions.get('window').width;
}

export function getScreenHeight(){
    return Dimensions.get('window').height;
}

export function getPixelRatio() {
    return PixelRatio.get();
}

export function getToolbarHeight() {
    if(Platform.OS=='ios'){
        return toolbarHeight_ios;
    }
    return toolbarHeight_android;
}

export function getToolbarPaddingTop() {
    if(Platform.OS=='ios'){
        return toolbarPaddingTop_ios;
    }
    return toolbarPaddingTop_android;
}

export function getToolbarPaddingBottom() {
    if(Platform.OS=='ios'){
        return toolbarPaddingBottom_ios;
    }
    return toolbarPaddingBottom_android;
}
